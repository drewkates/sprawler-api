liquibase \
 --driver=org.postgresql.Driver \
 --classpath=./liquibase/lib/postgresql-42.2.5.jar \
 --username=$USR \
 --password=$PASS \
 --url="jdbc:postgresql://$HOST:5432/$DB" \
 --changeLogFile="./liquibase/database_change_log.xml" \
 update
