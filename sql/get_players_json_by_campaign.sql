WITH players AS (
  SELECT p.player_name, p.id AS "playerId"
  FROM campaign c INNER JOIN player p ON p.id = ANY(c.players) )
SELECT json_build_object(
  'campaignId', c.id,
  'campaign', c.campaign_name,
  'players', json_agg(ps.*)
  ) FROM players ps, campaign c WHERE c.id=$1
GROUP BY c.campaign_name, c.id;