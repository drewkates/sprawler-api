UPDATE player
SET moves = (WITH ids as (SELECT id from move WHERE move.move_name = ANY(ARRAY['Ear to the ground','It all fits together', 'See the angles', 'Eye for detail'])) SELECT ARRAY (SELECT * from ids))
WHERE player_name='Frost';
-- select * from move
-- ORDER BY move_name;

-- INSERT INTO move (move_name, associated_playbooks) VALUES ('Ear to the ground', ARRAY['hunter'])



-- set links to all that they're the "from_player"
UPDATE player
SET links = (WITH ids as (SELECT links.id from links, player where links.from_player = player.id AND player.player_name = 'Frost') SELECT ARRAY (SELECT * from ids))
WHERE player_name = 'Frost';

select links from player;

WITH
plids AS (
  SELECT links.id FROM links,player WHERE links.from_player = player.id AND player.id = $1
  ),
lids AS (
  SELECT to_player, quant FROM links WHERE links.id = ANY(SELECT id from plids)
  )
SELECT player_name, quant from player,lids WHERE player.id = lids.to_player