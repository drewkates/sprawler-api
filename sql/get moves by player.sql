WITH playermoves as (
        SELECT moves from player p
        WHERE p.id = '3fc9d55a-a6ba-406d-b7ee-7aed7d7c5245'::uuid
      )
      SELECT id, move_name FROM
      move mv, playermoves
      WHERE mv.id = ANY(playermoves.moves)


    INSERT INTO public.vehicle(
            tags, player_id, id, name, frame, design, profile, strengths,
            looks, weaknesses, weapons, created_at, updated_at)
    VALUES (
    ?, ?, ?, ?, ?, ?, ?, ?,
            ?, ?, ?, ?, ?
            );
