## 1.1.3 (2019-04-27)


### Bug Fixes

* **Campaign:** Normalize the response to meet the contract from client ([4108db7](https://bitbucket.org/drewkates/sprawler-api/commits/4108db7))
* **message.route:** fix the insert to get the name from db before ([832d7ac](https://bitbucket.org/drewkates/sprawler-api/commits/832d7ac))
* **msg.route:** reverse sort order on back-end before sending, but after pulling "last" ([dc21ee5](https://bitbucket.org/drewkates/sprawler-api/commits/dc21ee5))
* **Rolls:** Fix bug where edge case threw 500 error ([9644dc6](https://bitbucket.org/drewkates/sprawler-api/commits/9644dc6))
* **scripts:** Fix update_db to use proper classpath ([7427caf](https://bitbucket.org/drewkates/sprawler-api/commits/7427caf))


### Features

* **CampaignRoute:** adds querying to populate player ids with player data ([32cf65f](https://bitbucket.org/drewkates/sprawler-api/commits/32cf65f))
* **db:** Adds listening capability to roll table ([0121f90](https://bitbucket.org/drewkates/sprawler-api/commits/0121f90))
* **DB:** Add SQL for messages table ([0c27bd3](https://bitbucket.org/drewkates/sprawler-api/commits/0c27bd3))
* **Liquibase:** Fix DDL scripts ([a04a6f2](https://bitbucket.org/drewkates/sprawler-api/commits/a04a6f2))
* **Roll:** Add roll_move_name to roll table ([0755382](https://bitbucket.org/drewkates/sprawler-api/commits/0755382))
* **Roll:** Adds pagination ([0939c9d](https://bitbucket.org/drewkates/sprawler-api/commits/0939c9d))
* **Roll/Msg:** Select with only player id ([067895b](https://bitbucket.org/drewkates/sprawler-api/commits/067895b))
* **rolls:** Adds socket firing when new roll entered into db ([0acacad](https://bitbucket.org/drewkates/sprawler-api/commits/0acacad))



