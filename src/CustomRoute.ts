import { Router } from 'express';
import { client } from './dbClient';
import express from 'express';
import { Client } from 'pg';

export class CustomRouter {
  public db: Client = client;
  public customRoute: Router;
  constructor(router: Router) {
    this.customRoute = router;
    this.customRoute.use(express.json());
  }
  public sendError(str: string, err?: Error): void {
    console.error(str, err);
  }
}
