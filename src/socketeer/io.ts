import io, { Socket, Server, Namespace } from 'socket.io';
import { Server as HttpServer } from 'http';

export class Io {
  public ref: Server | undefined;
  public rollNS: Namespace | undefined;
  public messageNS: Namespace | undefined;
  public getRef() {
    return this.ref;
  }
  public getRollNamespace() {
    return this.rollNS;
  }
  public getMessageNamespace() {
    return this.messageNS;
  }
  public init(server: HttpServer) {
    this.ref = io(server, {
      origins: ['http://localhost:1234', 'https://sprawler-api.herokuapp.com:*']
    });
    const okHost = process.env.APP_HOST + '*' || 'http://localhost:1234';
    this.ref.origins(okHost);
    this.rollNS = this.ref.of('/rolls');
    this.messageNS = this.ref.of('/messages');
  }
}

export default new Io();
