import { CustomRouter } from '../../CustomRoute';
import { Router } from 'express';

const move = new CustomRouter(Router());

move.customRoute.get('/move', async (req, res) => {
  const queryParams = req.query;

  const limitByPlaybook = !!queryParams.playbook;
  const where = limitByPlaybook
    ? `WHERE $1 = ANY(move.associated_playbooks)`
    : `WHERE true`;
  const query = `
    SELECT *
    FROM move
    ${where}
    `;
  const params = limitByPlaybook ? [queryParams.playbook] : void 0;
  const result = await move.db.query(query, params);
  res.json(result.rows);
});

move.customRoute.get('/move/:id', async (req, res) => {
  const id = req.params.id || '';
  try {
    const result = await move.db.query(
      `
      SELECT * FROM
      move mv
      WHERE mv.id = $1::uuid;
      `,
      [id]
    );
    if (!result || !result.rows || !result.rows.length) {
      res.statusCode = 404;
      return res.send({ error: 'Could not find move with id: ' + id });
    }

    const rows = result.rows;
    res.statusCode = 200;
    res.json(rows);
  } catch (error) {
    res.statusCode = 500;
    console.error(error);
    res.send({ error: 'query failed' });
  }
});

export const moveRoute = move.customRoute;
