import { Router, Request, Response } from 'express';
import { client } from '../../dbClient';
import { QueryConfig } from 'pg';
import express from 'express';
export const router = Router();
router.use(express.json());

const queryMap = new Map<string, QueryConfig>();
queryMap.set('move', {
  name: 'moves',
  text: `WITH playermoves as (
  SELECT moves from player p
  WHERE p.id = $1::uuid
)
SELECT move_name, id
FROM move mv, playermoves pm
WHERE mv.id = ANY(pm.moves)`,
  values: []
});
queryMap.set('cyberware', {
  name: 'cyberware',
  text: `
    SELECT *
      FROM cyberware cw
      WHERE cw.player_id = $1;`,
  values: []
});

queryMap.set('link', {
  name: 'link',
  text: `
  WITH
  plids AS (
    SELECT links.id FROM links,player WHERE links.from_player = player.id AND player.id = $1
    ),
  lids AS (
    SELECT to_player, quant FROM links WHERE links.id = ANY(SELECT id from plids)
    )
  SELECT player_name as "playerName", quant, player.id from player,lids WHERE player.id = lids.to_player
  `,
  values: []
});
queryMap.set('player', {
  name: 'player',
  text: 'SELECT * FROM player WHERE id = $1::uuid ',
  values: []
});
queryMap.set('directive', {
  name: 'directive',
  text: `
  SELECT
  id,
  details,
  directive_name as "directiveName"
 FROM directive WHERE player_id = $1;
   `,
  values: []
});

router.get('/player', async (req, res) => {
  const nameOnly = req.query.nameOnly === 'true';
  const cols = nameOnly ? `id, player_name` : `*`;
  const query = `SELECT ${cols} from player`;

  const result = await client.query(query);
  res.json(result.rows);
});
router.get('/player/:id/full', async (req, res) => {
  const id = req.params.id || '';
  if (!id) {
    res.status(400);
    return;
  }

  const tables = ['player', 'move', 'cyberware', 'link', 'directive'];

  const results = await Promise.all(
    tables
      .filter(tableName => queryMap.has(tableName))
      .map(tableName => {
        const query = queryMap.get(tableName);
        if (query === undefined) {
          throw Error('query should not be undefined');
        } else {
          query.values = [id];
          return client.query(query);
        }
      })
  );

  const resultsAsJson = results
    .map(item => item.rows)
    .reduce((aggregator: { [key: string]: any[] }, currentValue, index) => {
      const tableName = tables[index] || '';
      aggregator[tableName] = currentValue;
      return aggregator;
    }, {});

  res.json(resultsAsJson);
});

router.get('/player/:id', async (req, res) => {
  const userId: string = req.params.id || '';

  try {
    const result = await client.query(
      'SELECT * FROM player WHERE id = $1::uuid ',
      [userId]
    );
    if (!result || !result.rows) {
      res.statusCode = 404;
      return res.send({ error: 'Could not find character with id: ' + userId });
    }
    res.json(result.rows[0]);
  } catch (error) {
    res.statusCode = 500;
    res.send({ error: 'query failed' });
  }
});

router.get('/player/:id/move', async (req, res) => {
  const userId: string = req.params.id || '';

  try {
    const result = await client.query(queryMap.get('move') || '', [userId]);
    if (!result || !result.rows) {
      res.statusCode = 404;
      return res.send({ error: 'Could not find character with id: ' + userId });
    }
    res.json(result.rows);
  } catch (error) {
    res.statusCode = 500;
    res.send({ error: 'query failed' });
  }
});

router.get('/player/:id/cyberware', async (req, res) => {
  const userId: string = req.params.id || '';

  try {
    const query = queryMap.get('cyberware') || '';
    const result = await client.query(query, [userId]);
    if (!result || !result.rows) {
      res.statusCode = 404;
      return res.send({ error: 'Could not find character with id: ' + userId });
    }

    res.json(result.rows);
  } catch (error) {
    res.statusCode = 500;
    res.send({ error: 'query failed' });
  }
});

router.get('/player/:id/links', async (req, res) => {
  const userId: string = req.params.id || '';

  try {
    const query = queryMap.get('link') || '';
    const result = await client.query(query, [userId]);
    if (!result || !result.rows) {
      res.statusCode = 404;
      return res.send({ error: 'Could not find character with id: ' + userId });
    }

    res.json(result.rows);
  } catch (error) {
    res.statusCode = 500;
    res.send({ error: 'query failed' });
  }
});

router.get('/player/:id/directive', async (req, res) => {
  const userId: string = req.params.id || '';

  try {
    const result = await client.query(
      `
     SELECT
     id,
     details,
     directive_name as "directiveName"
    FROM directive WHERE player_id = $1;
      `,
      [userId]
    );
    if (!result || !result.rows) {
      res.statusCode = 404;
      return res.send({
        error: 'Could not find any directives with id: ' + userId
      });
    }

    res.json(result.rows);
  } catch (error) {
    res.statusCode = 500;
    res.send({ error: 'query failed' });
  }
});

router.get('/player/:id/weapon', async (req, res) => {
  const userId: string = req.params.id || '';

  try {
    const result = await client.query(
      `
        SELECT id,
          range_tag as "rangeTag",
          other_tags as tags,
          harm_rating as "harmRating",
          weapon_name as "name"
        FROM weapon WHERE owner_id = $1;
      `,
      [userId]
    );
    if (!result || !result.rows) {
      res.statusCode = 404;
      return res.send({ error: 'Could not find character with id: ' + userId });
    }
    res.json(result.rows);
  } catch (error) {
    res.statusCode = 500;
    res.send({ error: 'query failed' });
  }
});

router.get('/player/:id/contact', async (req, res) => {
  const userId: string = req.params.id || '';

  try {
    const result = await client.query(
      `
        SELECT
          id,
          contact_name,
          contact_description,
          contact_background
        FROM contact WHERE $1 = ANY(contact.associated_players);
      `,
      [userId]
    );
    if (!result || !result.rows) {
      res.statusCode = 404;
      return res.send({ error: 'Could not find character with id: ' + userId });
    }
    res.json(result.rows);
  } catch (error) {
    res.statusCode = 500;
    res.send({ error: 'query failed' });
  }
});

router.get('/player/:id/gang', async (req, res) => {
  const userId: string = req.params.id || '';

  try {
    const result = await client.query(
      `
        SELECT
          id,
          gang_size as "gangSize",
          gang_type as "gangType",
          details,
          gang_name as "gangName"
        FROM gang WHERE $1 = gang.player_id;
      `,
      [userId]
    );
    if (!result || !result.rows) {
      res.statusCode = 404;
      return res.send({
        error: 'Could not find gangsfor player id: ' + userId
      });
    }
    res.json(result.rows);
  } catch (error) {
    res.statusCode = 500;
    res.send({ error: 'query failed' });
  }
});

router.get('/player/:id/crew', async (req, res) => {
  const userId: string = req.params.id || '';

  try {
    const result = await client.query(
      `
        SELECT
          id,
          crew_name as "crewName",
          crew_type as "crewType",
          disaster,
          profit
        FROM crew WHERE $1 = ANY(crew.associated_players);
      `,
      [userId]
    );
    if (!result || !result.rows) {
      res.statusCode = 404;
      return res.send({
        error: 'Could not find crews for player id: ' + userId
      });
    }
    res.json(result.rows);
  } catch (error) {
    res.statusCode = 500;
    res.send({ error: 'query failed' });
  }
});

router.get('/player/:id/vehicle', async (req, res) => {
  const userId: string = req.params.id || '';

  try {
    const result = await client.query(
      `
        SELECT *
        FROM vehicle WHERE player_id = $1;
      `,
      [userId]
    );
    if (!result || !result.rows) {
      res.statusCode = 404;
      return res.send({ error: 'Could not find resource with id: ' + userId });
    }
    res.json(result.rows);
  } catch (error) {
    res.statusCode = 500;
    res.send({ error: 'query failed' });
  }
});

router.get('/player/:id/drone', async (req, res) => {
  const userId: string = req.params.id || '';

  try {
    const result = await client.query(
      `
        SELECT
        id,
        drone_name as "name",
        style,
        frame,
        strengths,
        sensors,
        weaknesses,
        armed,
        img_url as "imageURL"
        FROM drone WHERE player_id = $1;
      `,
      [userId]
    );
    if (!result || !result.rows) {
      res.statusCode = 404;
      return res.send({ error: 'Could not find drones with id: ' + userId });
    }
    res.json(result.rows);
  } catch (error) {
    console.error('error: drones ', error);
    res.statusCode = 500;
    res.send({ error: 'query failed' });
  }
});

// updates!
router.patch('/player/:id', async (req: Request, res: Response) => {
  const userId: string = req.params.id || '';
  try {
    const body = req.body;
    const keys = Object.keys(body) || [];
    const values = keys.map((key: string) => {
      const prop: any = body[key];
      if (typeof prop === 'string') {
        return `'${prop}'`;
      }
      if (typeof prop === 'number') {
        return prop;
      } else {
        return prop;
      }
    });

    const sets = keys.map((key, ind) => `${key} = ${values[ind]}`).join(',');
    const text = `
    UPDATE player
      SET ${sets}
    WHERE id = $1::uuid
    RETURNING *`;

    try {
      const result = await client.query(text, [userId]);
      res.json(result.rows[0]);
    } catch (e) {
      console.error('error updating player: ', e);
      res.statusCode = 500;
      res.send({ error: e });
    }
  } catch (e) {
    res.statusCode = 400;
    res.send({ error: 'invalid payload' });
  }
});
