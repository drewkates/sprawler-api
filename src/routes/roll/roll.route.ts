import { Router } from 'express';
import { client } from '../../dbClient';
import express from 'express';
export const router = Router();
router.use(express.json());

router.get('/rolls', async (req, res) => {
  const { limit = 25, campaignId, lastTimestamp, lastId } = req.query;
  if (!campaignId) {
    return res.status(404);
  }
  if (lastTimestamp && lastId) {
    try {
      const query = `
      SELECT *
      FROM roll
      WHERE roll.campaign_id = $1
        AND
        (roll.created_at, roll.id) <
        ($2::timestamptz, $3)
      ORDER BY roll.created_at DESC, roll.id DESC
      LIMIT $4;
      `;
      const args = [campaignId, lastTimestamp, lastId, limit];
      const allRolls = await client.query(query, args);
      const rolls = allRolls.rows;
      const lastRoll = rolls[rolls.length - 1];
      if (lastRoll) {
        return res.json({
          lastId: lastRoll.id,
          lastTimestamp: lastRoll.created_at,
          rolls
        });
      } else {
        return res.json({ lastId: null, lastTimestamp: null, rolls: [] });
      }
    } catch (error) {
      console.error('error: ', error);
      return res.sendStatus(500);
    }
  } else {
    try {
      const allRolls = await client.query(
        `
        SELECT * FROM roll
        WHERE campaign_id = $1
        ORDER BY created_at DESC
        LIMIT $2`,
        [campaignId, limit]
      );
      const rolls = allRolls.rows;
      const lastRoll = rolls[rolls.length - 1];
      return res.json({
        lastId: lastRoll.id,
        lastTimestamp: lastRoll.created_at,
        rolls
      });
    } catch (error) {
      console.error('error: ', error);
      return res.sendStatus(500);
    }
  }
});

router.post('/rolls', async (req, res) => {
  const { body } = req;

  try {
    const {
      rollType,
      rollMod,
      rollValues,
      playerId,
      moveName = body.rollType
    } = body;
    if (!playerId || playerId.length < 36) {
      return res.sendStatus(400);
    }
    const { rows: idRows } = await client.query(
      `SELECT id from campaign where $1 = ANY(players);`,
      [playerId]
    );
    const campaignId: string = idRows.pop().id;
    const insertValues = [
      rollType,
      rollMod,
      moveName,
      rollValues,
      playerId,
      campaignId
    ];
    const query = `
      INSERT INTO public.roll(
        roll_type,
        roll_mod,
        roll_move_name,
        "values",
        player_id,
        campaign_id
      )
        VALUES ($1, $2, $3, $4, $5, $6);
      `;
    try {
      const result = await client.query(query, insertValues);
      res.json(result.rows);
    } catch (error) {
      console.error('error: ', error);
      return res.sendStatus(400);
    }
  } catch (error) {
    console.error('error', error);
    return res.sendStatus(500);
  }
});
