import { Router } from 'express';
import { client } from '../../dbClient';
import express from 'express';
export const router = Router();
router.use(express.json());

router.get('/campaign', async (req, res) => {
  const result = await client.query('SELECT id, campaign_name as "campaignName" from campaign');
  res.json(result.rows);
});

router.get('/campaign/:id', async (req, res) => {
  const id = req.params.id || '';
  try {
    const result = await client.query(
      `
      WITH players AS (
        SELECT p.player_name as "playerName", p.id AS "playerId"
        FROM campaign c INNER JOIN player p ON p.campaign_id = c.id
      )
      SELECT id, campaign_name as "campaignName", json_agg(players.*) as players
      FROM players, campaign c WHERE c.id=$1
      GROUP BY c.campaign_name, c.id;
      `,
      [id]
    );
    if (!result || !result.rows || !result.rows.length) {
      res.statusCode = 404;
      return res.send({ error: 'Could not find move with id: ' + id });
    }

    const rows = result.rows;
    return res.json(rows[0]);
  } catch (error) {
    console.error(error);
    res.statusCode = 500;
    res.json({ error: 'query failed' });
  }
});
