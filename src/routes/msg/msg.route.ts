import { Router } from 'express';
import { client } from '../../dbClient';
import express from 'express';
export const router = Router();
router.use(express.json());

router.get('/messages', async (req, res) => {
  const { limit = 25, campaignId, lastTimestamp, lastId } = req.query;
  if (!campaignId) {
    return res.status(404);
  }
  if (lastTimestamp && lastId) {
    try {
      const query = `
      SELECT id, created_at, player_name, message_text
      FROM message
      WHERE campaign_id = $1
      AND
      (created_at, id) <
      ($2::timestamptz, $3)
      ORDER BY created_at DESC, id DESC
      LIMIT $4;
      `;
      const args = [campaignId, lastTimestamp, lastId, limit];
      const allMsgs = await client.query(query, args);
      const msgs = allMsgs.rows;
      const lastMsg = msgs[msgs.length - 1];
      if (lastMsg) {
        return res.json({
          lastId: lastMsg.id,
          lastTimestamp: lastMsg.created_at,
          messages: msgs.reverse()
        });
      } else {
        return res.json({ lastId: null, lastTimestamp: null, messages: [] });
      }
    } catch (error) {
      console.error('error: ', error);
      return res.sendStatus(500);
    }
  } else {
    try {
      const allMsgs = await client.query(
        `
        SELECT id, created_at, player_name, message_text
        FROM message
        WHERE campaign_id = $1
        ORDER BY created_at DESC
        LIMIT $2`,
        [campaignId, limit]
      );
      const messages = allMsgs.rows;

      // short-circuit w/ null state when no messages for campaign
      if (messages.length === 0) {
        return res.json({ lastId: null, lastTimestamp: '', messages: [] });
      }

      const lastMsg = messages[messages.length - 1];
      if (lastMsg) {
        return res.json({
          lastId: lastMsg.id,
          lastTimestamp: lastMsg.created_at,
          messages: messages.reverse()
        });
      }
    } catch (error) {
      console.error('error: ', error);
      return res.sendStatus(500);
    }
  }
});

router.post('/messages', async (req, res) => {
  const { body } = req;

  try {
    const { messageText, playerName, campaignId } = body;

    const insertValues = [messageText, campaignId, playerName];
    const query = `
      INSERT INTO public.message(
        message_text,
        campaign_id,
        player_name
      )
        VALUES ($1, $2, $3)
        RETURNING *;
      `;
    try {
      const insertResult = await client.query(query, insertValues);
      res.statusCode = 201;
      const result = insertResult.rows.length ? insertResult.rows.pop() : {};
      res.json({ ...result });
    } catch (error) {
      console.error('error: ', error);
      return res.sendStatus(500);
    }
  } catch (error) {
    console.error('error', error);
    return res.sendStatus(500);
  }
});
