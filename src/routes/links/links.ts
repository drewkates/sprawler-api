import { CustomRouter } from '../../CustomRoute';
import { Router } from 'express';

interface Links {
  from_player: string;
  to_player: string;
  id: string;
}
const links = new CustomRouter(Router());

links.customRoute.get('/links/:id', async (req, res) => {
  const id = req.params.id || '';
  try {
    const result = await links.db.query(
      `
      SELECT * FROM
      links l
      WHERE l.from_player = $1::uuid;
      `,
      [id]
    );
    if (!result || !result.rows || !result.rows.length) {
      res.statusCode = 404;
      return res.send({ error: 'Could not find links with id: ' + id });
    }

    const rows = result.rows;
    res.json(rows);
  } catch (error) {
    res.statusCode = 500;
    console.error(error);
    res.send({ error: 'query failed' });
  }
});

export const linksRoute = links.customRoute;
