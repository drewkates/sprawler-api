import express from 'express';
import cors from 'cors';
import { config } from 'dotenv';
import { client } from './dbClient';
import { router as PlayerRouter } from './routes/player';
import { router as RollRouter } from './routes/roll';
import { router as CampaignRouter } from './routes/campaign';
import { router as MessagesRouter } from './routes/msg';
import { move } from './routes/move';
import Io from './socketeer/io';
import { Notification } from 'pg';

config();

const app = express();

app.use(cors());
app.use(PlayerRouter);
app.use(RollRouter);
app.use(CampaignRouter);
app.use(MessagesRouter);
app.use(move);

const PORT = process.env.PORT || 8080;
const serverRef = app.listen(PORT, async () => {
  Io.init(serverRef);
  console.info('server started on port: ', PORT);
  try {
    await client.connect();
    console.info('connected to db');

    client.on('notification', (msg: Notification) => {
      if (Io.rollNS && Io.messageNS) {
        switch (msg.channel) {
          case 'watch_roll': {
            Io.rollNS.emit('roll:added', msg.payload);
            break;
          }
          case 'watch_message': {
            Io.messageNS.emit('msg:added', msg.payload);
            break;
          }
          default:
            console.error('no channel found for: ', msg.channel);
            break;
        }
      }
    });
    await client.query(`LISTEN watch_roll;`);
    await client.query(`LISTEN watch_message;`);
  } catch (error) {
    console.error(`DB failed with message: ${error}`);
  }
});

process.on('exit', () => {
  console.info('ending client connection');
  client.end();
});
