import { Client } from 'pg';
import { config } from 'dotenv';
config();
const dbURL = process.env.DATABASE_URL || '';

if (!dbURL) {
  throw Error('DATABASE URL NOT FOUND');
}
export const client = new Client({ ssl: true, connectionString: dbURL });
