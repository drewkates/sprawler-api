export interface CyberWare {
  name: string;
  details: any;
  tags: string[];
}
export interface ClientMove {
  name: string;
  notes: any;
}

export interface DBMove {
  id: string;
  move_name: string;
  associated_playbooks: string[];
}

export interface Characteristics {
  eyes: string;
  face: string;
  body: string;
  wear: string;
  skin: string;
  img_url?: string;
}
export interface Attributes {
  cool: number;
  edge: number;
  meat: number;
  mind: number;
  style: number;
  synth: number;
}
export type CharSheetState = 'read' | 'edit';

export interface ContactDB {
  contact_name: string;
  contact_description: string;
  contact_background: string;
  id: string;
}
export interface ContactDetails {
  name: string;
  description: string;
  background: string;
  gang_affiliation?: string;
}

export interface Gang {
  [key: string]: string;
  id: string;
  gangSize: string;
  gangType: string;
  details: string;
  gangName: string;
}

export interface Crew {
  disaster: string;
  crewName: string;
  crewType: string;
  id: string;
  profit: string;
}

export interface Drone {
  name: string;
  style: string;
  frame: string;
  strengths: string[];
  sensors: string[];
  weaknesses: string[];
  armed: string;
  imageURL?: string;
}

export interface Gear {
  details: string;
}

export interface Vehicle {
  id: string;
  tags: string[];
  name: string;
  frame: string;
  design: string;
  looks: string[];
  strengths: string[];
  weaknesses: string[];
  profile?: string;
  weapons: string[];
}

export interface Character extends Characteristics, Attributes {
  cred: number;
  contacts: ContactDB[];
  cyberware: CyberWare[];
  directives: Directive[];
  drones: Drone[];
  experience: number;
  crews: Crew[];
  gangs: Gang[];
  gear: Gear[];
  harm: string;
  id: string;
  links: LinksDB[];
  moves: ClientMove[];
  playbook: string;
  player_name: string;
  vehicles: Vehicle[];
  weapons: Weapon[];
}
export interface LinksDB {
  playerName: string;
  quant: number;
}

export interface LinksClient {
  fromPlayer: string;
  toPlayer: string;
}

export interface Weapon {
  tags: string[];
  harmRating: string;
  rangeTag: string;
  name: string;
}

export interface Directive {
  directiveName: string;
  details: string;
  id: string;
}
