--liquibase formatted sql
--changeset drewkakes:5 runOnChange:true splitStatements:false
CREATE OR REPLACE FUNCTION notify_roll_trigger() RETURNS trigger AS $$
DECLARE
BEGIN
  PERFORM pg_notify('watch_roll', row_to_json(NEW)::text);
  RETURN new;
END;
$$ LANGUAGE plpgsql;