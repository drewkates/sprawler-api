--liquibase formatted sql
--changeset drewkakes:3 runOnChange:true splitStatements:true
DROP TRIGGER IF EXISTS watch_roll_trigger ON roll;

CREATE TRIGGER watch_roll_trigger AFTER INSERT ON roll
FOR EACH ROW EXECUTE PROCEDURE notify_roll_trigger();