--liquibase formatted sql

--changeset drewkakes:1548103186594-21 runOnChange:true
CREATE EXTENSION IF NOT EXISTS pgcrypto;

--changeset drewkakes:1548103186594-1 runOnChange:true
CREATE TABLE IF NOT EXISTS campaign (
  campaign_name TEXT,
  id UUID DEFAULT gen_random_uuid() NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  players _UUID DEFAULT ARRAY[]::uuid[],
  CONSTRAINT campaign_pkey PRIMARY KEY (id)
  );

--changeset drewkakes:1548103186594-2 runOnChange:true
CREATE TABLE IF NOT EXISTS cyberware (
  tags _TEXT,
  player_id UUID,
  id UUID DEFAULT gen_random_uuid() NOT NULL,
  name TEXT,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  CONSTRAINT cyberware_pkey PRIMARY KEY (id)
);

--changeset drewkakes:1548103186594-3 runOnChange:true
CREATE TABLE IF NOT EXISTS cyberware_details (name TEXT,
  details TEXT,
  CONSTRAINT cyberware_details_name_key UNIQUE (name)
);

--changeset drewkakes:1548103186594-4 runOnChange:true
CREATE TABLE IF NOT EXISTS cyberware_tags (
  name TEXT,
  details TEXT,
  CONSTRAINT cyberware_tags_name_key UNIQUE (name)
);

--changeset drewkakes:1548103186594-5 runOnChange:true
CREATE TABLE IF NOT EXISTS directive (
  id UUID DEFAULT gen_random_uuid() NOT NULL,
  player_id UUID,
  directive_name TEXT,
  details TEXT,
  CONSTRAINT directive_pkey PRIMARY KEY (id)
);

--changeset drewkakes:1548103186594-6 runOnChange:true
CREATE TABLE IF NOT EXISTS drone (
  player_id UUID,
  id UUID DEFAULT gen_random_uuid() NOT NULL,
  motive_style TEXT,
  frame TEXT,
  strengths _TEXT DEFAULT ARRAY[]::text[],
  sensors _TEXT DEFAULT ARRAY[]::text[],
  weaknesses _TEXT DEFAULT ARRAY[]::text[],
  weapon UUID,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  CONSTRAINT drone_pkey PRIMARY KEY (id)
);

--changeset drewkakes:1548103186594-7 runOnChange:true
CREATE TABLE IF NOT EXISTS gang (
  gang_size TEXT,
  gang_type TEXT,
  details TEXT,
  player_id UUID,
  campaign_id UUID,
  id UUID DEFAULT gen_random_uuid() NOT NULL,
  gang_name TEXT,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  CONSTRAINT gang_pkey PRIMARY KEY (id),
  CONSTRAINT campaign_id FOREIGN KEY (campaign_id) REFERENCES campaign (id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

--changeset drewkakes:1548103186594-8 runOnChange:true
CREATE TABLE IF NOT EXISTS links (
  from_player UUID NOT NULL,
  to_player UUID NOT NULL,
  id UUID DEFAULT gen_random_uuid() NOT NULL,
  quant INTEGER NOT NULL,
  CONSTRAINT links_pkey PRIMARY KEY (id)
);

--changeset drewkakes:1548103186594-9 runOnChange:true
CREATE TABLE IF NOT EXISTS move (
  id UUID DEFAULT gen_random_uuid() NOT NULL,
  move_name TEXT,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  associated_playbooks _TEXT DEFAULT ARRAY[]::text[],
  CONSTRAINT move_pkey PRIMARY KEY (id)
);

--changeset drewkakes:1548103186594-10 runOnChange:true
CREATE TABLE IF NOT EXISTS player (
  campaign_id UUID,
  id UUID DEFAULT gen_random_uuid() NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  player_name TEXT,
  eyes TEXT,
  experience INTEGER,
  img_url TEXT,
  playbook TEXT,
  face TEXT,
  body TEXT,
  wear TEXT,
  skin TEXT,
  cool INTEGER,
  edge INTEGER,
  meat INTEGER,
  mind INTEGER,
  style INTEGER,
  synth INTEGER,
  intel _TEXT,
  gear _TEXT,
  cyberware _TEXT,
  contacts _UUID,
  vehicles _UUID,
  drones _UUID,
  crews _UUID,
  weapons _UUID,
  cred INTEGER,
  harm TEXT DEFAULT '12:00' NOT NULL,
  moves _UUID DEFAULT ARRAY[]::uuid[],
  links _UUID DEFAULT ARRAY[]::uuid[],
  CONSTRAINT player_pkey PRIMARY KEY (id),
  CONSTRAINT player_campaign_id_fkey FOREIGN KEY (campaign_id) REFERENCES campaign (id) ON UPDATE NO ACTION ON DELETE NO ACTION
);

--changeset drewkakes:1548103186594-11 runOnChange:true
CREATE TABLE IF NOT EXISTS user_account (
  username TEXT NOT NULL,
  email TEXT NOT NULL,
  name_first TEXT,
  name_last TEXT,
  pass_hash TEXT,
  id UUID DEFAULT gen_random_uuid() NOT NULL,
  role TEXT,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  CONSTRAINT user_account_pkey PRIMARY KEY (id),
  CONSTRAINT user_account_email_key UNIQUE (email)
);

--changeset drewkakes:1548103186594-12 runOnChange:true
CREATE TABLE IF NOT EXISTS vehicle (
  tags _TEXT,
  player_id UUID,
  id UUID DEFAULT gen_random_uuid() NOT NULL,
  name TEXT,
  frame TEXT,
  design TEXT,
  profile TEXT,
  strengths _TEXT DEFAULT ARRAY[]::text[],
  looks _TEXT DEFAULT ARRAY[]::text[],
  weaknesses _TEXT DEFAULT ARRAY[]::text[],
  weapons _UUID DEFAULT ARRAY[]::uuid[],
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  CONSTRAINT vehicle_pkey PRIMARY KEY (id)
);

--changeset drewkakes:1548103186594-13 runOnChange:true
CREATE TABLE IF NOT EXISTS weapon (
  other_tags _TEXT,
  harm_rating TEXT,
  range_tag TEXT,
  owner_id UUID,
  id UUID DEFAULT gen_random_uuid() NOT NULL,
  weapon_name TEXT,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  CONSTRAINT weapon_pkey PRIMARY KEY (id),
  CONSTRAINT owner_id FOREIGN KEY (owner_id) REFERENCES player (id) ON UPDATE NO ACTION ON DELETE NO ACTION
);
