--liquibase formatted sql
--changeset drewkakes:2 runOnChange:true splitStatements:false
CREATE OR REPLACE FUNCTION notify_message_trigger() RETURNS trigger AS $$
DECLARE
BEGIN
  PERFORM pg_notify('watch_message', row_to_json(NEW)::text);
  RETURN new;
END;
$$ LANGUAGE plpgsql;
