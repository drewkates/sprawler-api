--liquibase formatted sql
--changeset drewkakes:2 runOnChange:true splitStatements:false
CREATE TABLE IF NOT EXISTS message (
  id UUID DEFAULT gen_random_uuid() NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  player_id UUID,
  player_name TEXT,
  campaign_id UUID,
  CONSTRAINT message_pkey PRIMARY KEY (id)
);

CREATE INDEX IF NOT EXISTS message_created_id_idx
    ON public.message USING btree
    (created_at DESC NULLS LAST, id DESC NULLS LAST)
    TABLESPACE pg_default;