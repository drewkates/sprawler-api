--liquibase formatted sql
--changeset drewkakes:1 runOnChange:true
ALTER TABLE public.message
    ADD COLUMN IF NOT EXISTS message_text text DEFAULT '';