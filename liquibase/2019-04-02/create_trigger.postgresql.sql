--liquibase formatted sql
--changeset drewkakes:2 runOnChange:true splitStatements:true
DROP TRIGGER IF EXISTS watch_message_trigger ON message;
CREATE TRIGGER watch_message_trigger AFTER INSERT ON message
FOR EACH ROW EXECUTE PROCEDURE notify_message_trigger();