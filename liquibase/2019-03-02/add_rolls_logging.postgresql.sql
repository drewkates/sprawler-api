--liquibase formatted sql
--changeset drewkakes:3 runOnChange:true
CREATE TABLE IF NOT EXISTS roll (
  id UUID DEFAULT gen_random_uuid() NOT NULL,
  roll_type TEXT,
  roll_mod INTEGER,
  values INTEGER[],
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  player_id UUID,
  user_acct_id UUID,
  campaign_id UUID,
  CONSTRAINT roll_pkey PRIMARY KEY (id)
);