--liquibase formatted sql

--changeset drewkakes:1 runOnChange:true

CREATE TABLE IF NOT EXISTS contact (
  contact_name TEXT,
  contact_description TEXT,
  contact_background TEXT,
  id UUID DEFAULT gen_random_uuid() NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  associated_players UUID[] DEFAULT ARRAY[]::uuid[],
  CONSTRAINT contact_pkey PRIMARY KEY (id)
);