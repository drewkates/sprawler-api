--liquibase formatted sql
--changeset drewkakes:1 runOnChange:true
CREATE INDEX IF NOT EXISTS roll_created_id_idx
    ON public.roll USING btree
    (created_at DESC NULLS LAST, id DESC NULLS LAST)
    TABLESPACE pg_default;