--liquibase formatted sql

--changeset drewkakes:2 runOnChange:true
CREATE TABLE IF NOT EXISTS drone (
  player_id UUID,
  id UUID DEFAULT gen_random_uuid() NOT NULL,
  drone_name TEXT,
  style TEXT,
  frame TEXT,
  strengths _TEXT DEFAULT ARRAY[]::text[],
  sensors _TEXT DEFAULT ARRAY[]::text[],
  weaknesses _TEXT DEFAULT ARRAY[]::text[],
  armed TEXT,
  img_url TEXT,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  CONSTRAINT drone_pkey PRIMARY KEY (id)
);