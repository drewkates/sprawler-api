--liquibase formatted sql

--changeset drewkakes:2 runOnChange:true
CREATE TABLE IF NOT EXISTS crew (
  crew_name TEXT,
  profit TEXT,
  crew_description TEXT,
  crew_type TEXT,
  disaster TEXT,
  id UUID DEFAULT gen_random_uuid() NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  updated_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  associated_players UUID[] DEFAULT ARRAY[]::uuid[],
  CONSTRAINT crew_pkey PRIMARY KEY (id)
);